var gulp = require('gulp'),
    args = require('yargs').argv,
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano'),
    browserify = require('browserify'),
    concat = require('gulp-concat'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    notify = require('gulp-notify'),
    path = require('path'),
    plumber = require('gulp-plumber'),
    postcss = require('gulp-postcss'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    svgmin = require('gulp-svgmin'),
    v_buffer = require('vinyl-buffer'),
    v_source = require('vinyl-source-stream');

let gulp_env = () => args.env || 'dev',
    bundle = () => args.b || 'frontend',
    sourcePath = () => path.join(__dirname,'assets',bundle(),'src'),
    errorHandler = (error) => {
        console.error(error);
        this.emit('end');
    };

/* Remove dist folder */
gulp.task('clear',() => del([path.normalize(path.join(sourcePath(),'../dist'))]));

/* Fonts */
gulp.task('fonts', () => {
  return gulp.src([path.join(sourcePath(),'/fonts/**/*.{woff,woff2,eot,svg,ttf,css}')])
    .pipe(plumber({errorHandler: errorHandler}))
    .pipe(gulp.dest(path.normalize(path.join(sourcePath(),'../dist/fonts'))));
});

/* Less compiler */
gulp.task('styles',() => {
    return gulp.src([path.join(sourcePath(),'scss/bootstrap.scss')])
        .pipe(plumber({errorHandler:errorHandler}))
        .pipe(sass({
            includePaths:[path.join(__dirname,'vendor/bower/bootstrap/scss')]
        }))
        .pipe(concat('all.css'))
        .pipe(postcss([
            autoprefixer({browsers:['last 2 version']}),
            cssnano()
        ]))
        .pipe(gulp.dest(path.normalize(path.join(sourcePath(),'../dist/css'))))
        .pipe(notify('CSS done!'));
});

/* Images */
gulp.task('images', () => {
    return gulp.src([path.join(sourcePath(),'/images/*.{gif,jpg,jpeg,png}')])
        .pipe(plumber({errorHandler: errorHandler}))
        .pipe(imagemin())
        .pipe(gulp.dest(path.normalize(path.join(sourcePath(),'../dist/images'))))
        .pipe(notify('Images done!'));
});
gulp.task('svg',()=>{
    return gulp.src([path.join(sourcePath(),'/images/*.svg')])
        .pipe(svgmin({plugins:[{
                cleanupNumericValues: {
                    floatPrecision: 2
                }
            },{
                removeDimensions:true
            },{
                convertColors: {
                    names2hex: false,
                    rgb2hex: false
                }
            }]
        }))
        .pipe(gulp.dest(path.normalize(path.join(sourcePath(),'../dist/images'))));
});

/* JS modules */
gulp.task('javascript', () =>{
    return browserify(path.join(sourcePath(),'/js/index.js'))
        .bundle()
        .pipe(v_source(path.join(sourcePath(),'/js/index.js')))
        .pipe(v_buffer())
        .pipe(rename(`all-${bundle()}.js`))
        //.pipe(uglify())
        .pipe(gulp.dest(path.normalize(path.join(sourcePath(),'../dist/js'))))
        .pipe(notify('Javascript done!'));
});

gulp.task('build',['styles','fonts','javascript','svg','images']);

/* Watch */
gulp.task('watch', ['build'], function() {
  gulp.watch([path.join(sourcePath(),'scss/**')], ['styles']);
  gulp.watch([path.join(sourcePath(),'images/*.{gif,jpg,jpeg,png}')], ['images']);
  gulp.watch([path.join(sourcePath(),'/js/**/*.js')], ['javascript']);
  //gulp.watch('basic/src/fonts/**/*.{woff,woff2,eot,svg,ttf,css}', ['fonts']);
  //gulp.watch('basic/dist/**/**.*').on('change',bs.reload);
});