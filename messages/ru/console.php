<?php
return [
    'Permisson to view content' => 'Разрешение для просмотра содержимого',
    'Permisson to create a new user' => 'Разрешение на добавление нового пользователя',
    'Permission to update user data and user profile'=>'Разрешение на редактирование учетных данных и профиля пользователя',
    'Permission to blocking an user'=>'Разрешение на блокировку пользователя',
    'Permission for the complete removal an user with all related data'=>'Разрешение на удаление пользователя из системы вместе со всеми связанными данными',
    'Permission to registration, updating users info and blocking user'=>'Групповое разрешение на управление пользователями',
    'Registered user' => 'Зарегистрированный пользователь',
    'The system administrator. Has access to content management and user management'=>'Системный администратор. Имеет доступ к управлению контентом и пользователями',
    'Supervisor - a user with root access'=>'Пользователь с полным доступом к системе управления'
];