<?php

return [
    'language'=>'ru',
    'components'=>[
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
            'defaultRoles' => ['User','Root']
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=yii2app_basic',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'i18n'=>[
            'translations'=>[
                '*'=>[
                    'class' => '\yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages'
                ]
            ]
        ],
        'mailer'=>[
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
        ]
    ]
];