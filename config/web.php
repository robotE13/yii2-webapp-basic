<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => 'frontend/main/index',
    'layout' => 'frontend',
    'components' => [
        'assetManager'=>[
            'forceCopy' => YII_DEBUG,
            'bundles' => YII_ENV == 'prod'? require (__DIR__ . DIRECTORY_SEPARATOR . 'asset-prod.php'):[
                'yii\bootstrap\BootstrapAsset'=>[
                    'sourcePath'=>null,
                    'css'=>[]
                ]
            ]
        ],
        'authManager'=>[
            'class'=> 'dektrium\rbac\components\DbManager',
            'cache'=>'cache'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'class'=> 'robote13\SEOTags\components\RedirectHandler',
            'errorAction' => 'frontend/main/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ''=>'frontend/main/index',
            ],
        ],
        'view' =>[
            'theme'=> 'app\themes\Theme'
        ]
    ],
    'modules'=>[
        'backend' => [
            'class' => 'app\modules\backend\Module',
            'layout' => '/backend',
            'as access' => [
                'class'=>'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'=>true,
                        'roles' => ['viewBackend']
                    ]
                ]
            ],
            'modules' => [
                'user' => [
                    'class' => 'dektrium\user\Module',
                    'adminPermission'=>'manageUsers',
                    'as backend' => 'dektrium\user\filters\BackendFilter',
                ],
                'rbac' => 'dektrium\rbac\RbacWebModule',
                'seo' => [
                    'class'=> 'robote13\SEOTags\Module',
                ],
            ],
        ],
        'frontend' => [
            'class' => 'app\modules\frontend\Module',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'adminPermission'=>'manageUsers',
            'as frontend' => 'dektrium\user\filters\FrontendFilter',
        ],
    ],
    'params' => $params,
];

return $config;
