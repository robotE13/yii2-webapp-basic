<?php

namespace app\modules\frontend;

class Module extends \yii\base\Module
{
    public $defaultRoute = 'main';

    public $controllerNamespace = 'app\modules\frontend\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
