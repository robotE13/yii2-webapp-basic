<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\rbac;

use Yii;
use yii\web\IdentityInterface;

/**
 * Description of SupervisorRoleRule
 *
 * @author Tartharia
 * @todo Надо добавить возможность находить identity если в массиве параметров не передан проверяемый пользователь,
 * а $user проверяется отличный от Yii::$app->user->identity->id
 */
class DefaultRoleRule extends \yii\rbac\Rule
{

    public function init() {
        $this->name = 'defaultRole';
    }

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params) {
        if(!Yii::$app->user->isGuest)
        {
            /* @var $identity \common\models\User */
            $identity = key_exists('user',$params) && $params['user'] instanceof IdentityInterface ? $params['user'] : Yii::$app->user->identity;

            switch ($item->name) {
                case 'User':
                    return count(Yii::$app->authManager->getAssignments($identity->id))===0?$identity->username !== 'root':false;
                case 'Root':
                    return $identity->username == 'root';
                default:
                    return false;
            }
        }
        return false;
    }
}
