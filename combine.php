<?php
/**
 * Configuration file for the "yii asset" console command.
 */

// In the console environment, some path aliases may not exist. Please define these:
 Yii::setAlias('@webroot', __DIR__ . '\web');
 Yii::setAlias('@web', '/');

return [
    // Adjust command/callback for JavaScript files compressing:
    'jsCompressor' => 'java -jar compiler.jar --js {from} --js_output_file {to}',
    // Adjust command/callback for CSS files compressing:
    'cssCompressor' => 'java -jar yuicompressor-2.4.7.jar --type css {from} -o {to}',
    // Whether to delete asset source after compression:
    'deleteSource' => false,
    // The list of asset bundles to compress:
    'bundles' => [
        'app\assets\FrontendAsset',
        'app\assets\BackendAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ],
    // Asset bundle for compression output:
    'targets' => [
        'shared' => [
            'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'js' => 'all-{hash}.js',
            'css' => 'all-{hash}.css',
            'depends'=>[
                'yii\web\YiiAsset',
                'yii\web\JqueryAsset',
                'yii\bootstrap\BootstrapAsset',
                'yii\bootstrap\BootstrapPluginAsset'
            ]
        ],
        'allFrontend' => [
            'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'js' => 'all-front-{hash}.js',
            'css' => 'all-front-{hash}.css',
            'depends'=>[
                'app\assets\FrontendAsset'
            ]
        ],
        'allBackend' => [
            'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'js' => 'all-back-{hash}.js',
            'css' => 'all-back-{hash}.css',
            'depends'=>[
                'app\assets\BackendAsset'
            ]
        ]
    ],
    // Asset manager configuration:
    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets',
        'bundles'=>[
            'yii\bootstrap\BootstrapAsset'=>[
                'sourcePath'=>null,
                'css'=>[]
            ]
        ]
    ],
];