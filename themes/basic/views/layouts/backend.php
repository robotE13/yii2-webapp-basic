<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\BackendAsset;

BackendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Backend',
        'brandUrl' => ['/backend/main/index'],
        'options' => [
            'class' => 'navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'activateParents'=>true,
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => Yii::t('user','Users'), 'items'=>[
                ['label' => Yii::t('user','Manage users'), 'url' => ['/backend/user/admin/index']],
                ['label' => Yii::t('user','Roles'), 'url' => ['/backend/rbac/role/index']],
                ['label' => Yii::t('user','Permissions'), 'url' => ['/backend/rbac/permission/index']],
                ['label' => Yii::t('user','Rules'), 'url' => ['/backend/rbac/rule/index']],
            ]],
            ['label' => Yii::t('robote13/seotags','SEO'), 'items'=>[
                ['label' => Yii::t('robote13/seotags','Meta tags manage'), 'url' => ['/backend/seo/meta-tags/index']]
            ]],
            ['label' => Yii::t('app','Frontend'), 'url' => ['/frontend/main/index']],
            Yii::$app->user->isGuest ? (
                ['label' => Yii::t('app', 'Login'), 'url' => ['/backend/user/security/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/backend/user/security/logout'], 'post')
                . Html::submitButton(
                    Yii::t('app','Logout ({username})',['username'=>Yii::$app->user->identity->username]),
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
