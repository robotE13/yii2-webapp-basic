<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\themes;

/**
 * Description of BackendTheme
 *
 * @author Tartharia
 */
class Theme extends \yii\base\Theme
{

    public function init()
    {
        $this->basePath = '@app/themes/basic';
        parent::init();
    }
}
