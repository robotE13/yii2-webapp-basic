<?php

use yii\db\Migration;

class m170208_181023_create_permissions_for_rbac extends Migration
{

    public function up()
    {
        $manager = Yii::$app->authManager;

        //Permission to view page
        $viewContent = $manager->createPermission('viewContent');
        $viewContent->description = Yii::t('console', 'Permisson to view content');
        $manager->add($viewContent);

        //Permission to backend access
        $viewBackend = $manager->createPermission('viewBackend');
        $viewBackend->description = Yii::t('console', 'Permisson to view backend');
        $manager->add($viewBackend);

        //Permissions to manage users
        $registerUser = $manager->createPermission('registerUser');
        $registerUser->description = Yii::t('console', 'Permisson to create a new user');
        $manager->add($registerUser);

        $updateUser = $manager->createPermission('updateUserData');
        $updateUser->description = Yii::t('console','Permission to update user data and user profile');
        $manager->add($updateUser);

        $blockUser = $manager->createPermission('blockUser');
        $blockUser->description = Yii::t('console','Permission to blocking an user');
        $manager->add($blockUser);

        $deleteUser = $manager->createPermission('deleteUser');
        $deleteUser->description = Yii::t('console','Permission for the complete removal an user with all related data');
        $manager->add($deleteUser);

        // Group permission to manage users
        $manageUsers = $manager->createPermission('manageUsers');
        $manageUsers->description = Yii::t('console','Permission to registration, updating users info and blocking user');
        $manager->add($manageUsers);
        $manager->addChild($manageUsers, $registerUser);
        $manager->addChild($manageUsers, $updateUser);
        $manager->addChild($manageUsers, $blockUser);

        //Permissions to manage system roles

        /* Roles */
        /* Add rule to default roles */
        $defaultRoleRule = new \app\rbac\DefaultRoleRule();
        $manager->add($defaultRoleRule);

        /* Base group roles*/
        $user = $manager->createRole('User');
        $user->description = Yii::t('console','Registered user');
        $user->ruleName = $defaultRoleRule->name;
        $manager->add($user);
        $manager->addChild($user, $viewContent);

        $admin = $manager->createRole('Administrator');
        $admin->description = Yii::t('console', 'The system administrator. Has access to content management and user management');
        $manager->add($admin);
        $manager->addChild($admin, $manageUsers);
        $manager->addChild($admin, $viewContent);
        $manager->addChild($admin, $viewBackend);


        $root = $manager->createRole('Root');
        $root->description = Yii::t('console', 'Supervisor - a user with root access');
        $root->ruleName = $defaultRoleRule->name;
        $manager->add($root);
        $manager->addChild($root, $admin);
        $manager->addChild($root, $deleteUser);
    }

    public function down()
    {
        Yii::$app->authManager->removeAll();
    }
}
