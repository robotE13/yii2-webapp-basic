<?php

use yii\db\Migration;
use yii\helpers\Console;
use dektrium\user\models\User;
use yii\base\InvalidParamException;
use yii\helpers\VarDumper;

class m160511_185322_create_root_user extends Migration
{
    public function up()
    {
        Yii::$app->setModule('user',['class'=> '\dektrium\user\Module','modelMap'=>['User'=> User::className()]]);

        $password = Console::prompt('Set a password for root access:',[
            'required'=>true,
            'pattern'=>'/(?=^.{8,16}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/',
            'error'=>"The password must contain uppercase and lowercase letters, numbers or special characters. Length 8 - 16 characters.\n"
        ]);

        $user = new User(['password'=>$password,'username'=>'root','email'=>'no-email@mail.info','scenario'=>'register']);
        if($user->validate() && $user->create())
        {
            echo "The root user has been created.\n To enter use the following data: login - 'root', password - {$user->password}\n";
        }else{
            throw new InvalidParamException(VarDumper::dumpAsString($user->getErrors()));
        }

    }

    public function down()
    {
        User::deleteAll(['username'=>'root']);
    }
}