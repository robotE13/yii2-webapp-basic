<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\assets;

/**
 * Description of FontelloAsset
 *
 * @author Tartharia
 */
class FontelloAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/assets/fontello/dist';

    public $css = [
        'css/fontello.css',
    ];

    public $js = [
    ];
}
